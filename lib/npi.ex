import Kernel
defmodule Npi.CLI do

  def main(args \\ []) do
    args
    |> parse_file()
    |> output()
    |> response()
    |> install()
  end

  defp parse_file(args) do
    File.stream!("nixpkgs.txt")
    |> Enum.map(&String.trim/1)
    |> Enum.filter(fn x -> String.contains?(x, args) end)
    |> Enum.map(fn x -> String.split(x, " ", trim: true) end)
  end

  defp output(list) do
    Enum.map(list, fn x -> Enum.take(x, 1) end)
    |> Enum.map(fn x -> Enum.join(x) end)
    |> Enum.sort(&(byte_size(&1) > byte_size(&2)))
    |> Enum.with_index
  end

  defp response(nixpkgs) do
    Enum.map(nixpkgs, fn x ->
      IO.puts "[#{ length(nixpkgs) - elem(x, 1)}]  #{elem(x, 0)} "
    end)

    {input, _} =
      IO.gets("<3 ")
      |> Integer.parse

    Enum.fetch!(nixpkgs, length(nixpkgs) - input)
    |> elem(0)

  end

  defp install(pkg) do
    String.to_charlist("nix-env -iA #{pkg}")
    |> :os.cmd
    |> IO.write
  end

end

